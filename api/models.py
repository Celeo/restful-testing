from api.shared import db
from passlib.apps import custom_app_context as pwd_context
from flask_restful import fields
from datetime import datetime
from binascii import hexlify
from os import urandom


class User(db.Model):
    __tablename__ = 'api_user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String(100))
    password = db.Column(db.String(200))
    api_tokens = db.relationship('ApiToken', backref='user', lazy='dynamic')
    records_created = db.relationship('Record', backref='user', lazy='dynamic')
    access_level = db.Column(db.Integer)

    resource_fields = {
        'id': fields.Integer,
        'name': fields.String
    }
    resource_fields_private = {
        'id': fields.Integer,
        'name': fields.String,
        'email': fields.String,
        'access_level': fields.Integer
    }

    def __init__(self, name, email, password):
        self.name = name
        self.email = email
        self.set_password_hash(password)
        self.access_level = 1

    def set_password_hash(self, password):
        if password:
            self.password = pwd_context.encrypt(password)

    def check_password(self, password):
        return pwd_context.verify(password, self.password)

    def generate_new_auth_token(self):
        token = ApiToken(self.id)
        db.session.add(token)
        db.session.commit()
        return token.token

    @staticmethod
    def verify_auth_token(token_code):
        api_token = ApiToken.query.filter_by(token=token_code).first()
        if not api_token:
            return False
        return api_token.user

    def __str__(self):
        return '<User {}>'.format(self.name)


class ApiToken(db.Model):
    __tablename__ = 'api_apitoken'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('api_user.id'))
    token = db.Column(db.String(100))
    date_created = db.Column(db.DateTime)

    resource_fields = {
        'id': fields.Integer,
        'user_id': fields.Integer,
        'token': fields.String,
        'date_created': fields.DateTime,
    }

    def __init__(self, user_id):
        self.user_id = user_id
        self.token = self._generate_token_value()
        self.date_created = datetime.now()

    def _generate_token_value(self):
        return hexlify(urandom(20)).decode('utf-8')


class Record(db.Model):
    __tablename__ = 'api_record'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200))
    creator_id = db.Column(db.Integer, db.ForeignKey('api_user.id'))
    text = db.Column(db.String)
    date_added = db.Column(db.DateTime)

    resource_fields = {
        'id': fields.Integer,
        'title': fields.String,
        'creator_id': fields.Integer,
        'date_added': fields.DateTime
    }

    resource_fields_body = {
        'id': fields.Integer,
        'title': fields.String,
        'creator_id': fields.Integer,
        'date_added': fields.DateTime,
        'text': fields.String
    }

    def __init__(self, title, creator_id, text=None, date_added=None, ):
        self.title = title
        self.creator_id = creator_id
        self.text = text
        self.date_added = date_added or datetime.now()
