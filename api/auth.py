from flask import make_response, jsonify, request, g
from functools import wraps
from api.models import User


class PERMISSIONS:
    """
    This static-like class contains the definitions of various permission
    levels for using with the @access decorator
    """
    user = 1
    mod = 2
    admin = 3
    owner = 4


def login_required(f):
    """ This decorator covers the calls to view and generate API tokens """
    @wraps(f)
    def wrapper(*args, **kwargs):
        if request.authorization and \
                all(e in request.authorization for e in ['username', 'password']):
            user = User.query.filter_by(name=request.authorization['username']).first()
            if user and user.check_password(request.authorization['password']):
                g.user = user
                return f(*args, **kwargs)
        print(request.authorization)
        return make_response(jsonify({'message': 'Authorization failed'}), 401)
    return wrapper


def token_required(f):
    """ This decorator covers hiding private information to unathenticated API calls """
    @wraps(f)
    def wrapper(*args, **kwargs):
        if request.authorization and 'username' in request.authorization:
            user = User.verify_auth_token(request.authorization['username'])
            if user:
                g.user = user
                return f(*args, **kwargs)
        return make_response(jsonify({'message': 'Authorization failed'}), 401)
    return wrapper


class access:
    """
    This class acts as a decorator that takes an int permission level
    and checks it against the calling user's permission level to restrict
    access to an endpoint to a smaller collection of users.
    """
    def __init__(self, level):
        self.level = int(level)

    def __call__(self, f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if not 'user' in g:
                return make_response(jsonify({'message': 'Authorization failed'}), 401)
            if g.user.access_level < self.level:
                return make_response(jsonify({'message': 'Permission denied'}), 403)
            return f(*args, **kwargs)
        return wrapper
