from flask import Flask
from flask_restful import Api
from datetime import timedelta
from api.shared import db, app_settings
from api.endpoints import UserListAPI, UserAPI, ApiTokenListAPI, ApiTokenAPI, RecordListAPI, RecordAPI


app = Flask(__name__)
app.config.from_pyfile('config.cfg')
for k, v in app.config.items():
    app_settings[k] = v
app.permanent_session_lifetime = timedelta(days=14)
db.app = app
db.init_app(app)
api = Api(app)


api.add_resource(UserListAPI, '/users')
api.add_resource(UserAPI, '/user/<int:id>')
api.add_resource(ApiTokenListAPI, '/user/tokens')
api.add_resource(ApiTokenAPI, '/user/tokens/<int:id>')
api.add_resource(RecordListAPI, '/records')
api.add_resource(RecordAPI, '/record/<int:id>')
