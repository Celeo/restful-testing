from flask import g
from flask_restful import Resource, marshal_with, reqparse
from api.models import User, Record, ApiToken
from api.shared import db
from api.auth import token_required, login_required, access, PERMISSIONS


class UserAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('email', type=str)
        self.reqparse.add_argument('password', type=str)
        super(UserAPI, self).__init__()

    @token_required
    @access(PERMISSIONS.user)
    @marshal_with(User.resource_fields_private)
    def get(self, id):
        user = User.query.get(id)
        if not user:
            return {'message': 'User not found'}, 404
        if not user.id == g.user.id:
            if not g.user.access_leel > 1:
                return {'message': 'You cannot view another user\'s details'}, 403
        return user

    @token_required
    @access(PERMISSIONS.user)
    @marshal_with(User.resource_fields_private)
    def put(self, id):
        user = User.query.get(id)
        if not user:
            return {'message': 'User not found'}, 404
        if not user.id == g.user.id:
            if not g.user.access_level > 2:
                return {'message': 'You cannot edit another user\'s details'}, 403
        args = self.reqparse.parse_args()
        if args['email']:
            user.email = args['email']
        if args['password']:
            user.set_password_hash(args['password'])
        db.session.commit()
        return user

    @token_required
    @access(PERMISSIONS.admin)
    def delete(self, id):
        db.session.delete(User.query.get(id))
        db.session.commit()
        return '', 204


class UserListAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('name', type=str)
        self.reqparse.add_argument('email', type=str)
        self.reqparse.add_argument('password', type=str)
        super(UserListAPI, self).__init__()

    @token_required
    @access(PERMISSIONS.mod)
    @marshal_with(User.resource_fields)
    def get(self):
        return User.query.all()

    @token_required
    @access(PERMISSIONS.admin)
    @marshal_with(User.resource_fields)
    def post(self):
        args = self.reqparse.parse_args()
        db.session.add(User(args['name'], args['email'], args['password']))
        db.session.commit()
        return User.query.all()


class ApiTokenListAPI(Resource):
    @login_required
    @marshal_with(ApiToken.resource_fields)
    def get(self):
        return ApiToken.query.filter_by(user_id=g.user.id).all()

    @login_required
    @marshal_with(ApiToken.resource_fields)
    def post(self):
        g.user.generate_new_auth_token()
        return ApiToken.query.filter_by(user_id=g.user.id).all()


class ApiTokenAPI(Resource):
    @token_required
    @access(PERMISSIONS.user)
    def delete(self, id):
        token = ApiToken.query.get(id)
        if not token:
            return {'message': 'Token not found'}, 404
        if not token.user_id == g.user.id:
            if not g.user.access_level > 1:
                return {'message', 'You are not the owner of that token'}, 403
        db.session.delete(token)
        db.session.commit()
        return '', 204


class RecordAPI(Resource):
    @token_required
    @access(PERMISSIONS.user)
    @marshal_with(Record.resource_fields_body)
    def get(self, id):
        return Record.query.get(id)

    @token_required
    @access(PERMISSIONS.mod)
    @marshal_with(Record.resource_fields)
    def put(self, id):
        return {'messsage': 'unimplemented'}

    @token_required
    @access(PERMISSIONS.mod)
    def delete(self, id):
        return {'messsage': 'unimplemented'}


class RecordListAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('title', type=str)
        self.reqparse.add_argument('text', type=str)
        super(RecordListAPI, self).__init__()

    @token_required
    @access(PERMISSIONS.user)
    @marshal_with(Record.resource_fields)
    def get(self):
        return Record.query.all()

    @token_required
    @access(PERMISSIONS.user)
    @marshal_with(Record.resource_fields)
    def post(self):
        args = self.reqparse.parse_args()
        db.session.add(Record(args['title'], g.user.id, args['text']))
        db.session.commit()
        return Record.query.all()
